# OSMData

This projects maintains and updates a `planet.osm.pbf` file for our usages. This is the base file containing all the data that is imported/updated in our services.  
This includes our `Vectortiles` and `Routing database` for foot, bicycle and car routes.  
The `Search data` is updated seperately using diffs only.  
  
## build

The build directory contains a `Dockerfile` for osmctools which is used to update the `.pbf` file with a replication server.